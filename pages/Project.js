import React, { Component } from 'react';
import { ScrollView, Dimensions, Image, View, Share, RefreshControl, TouchableWithoutFeedback, Geolocation, Platform, Clipboard } from 'react-native';

import { Text, H1, Left, Right, Icon, Container, Button, Content, Toast } from 'native-base';
import HTML from 'react-native-render-html';

import MapView, { PROVIDER_GOOGLE } from 'react-native-maps';
import MapViewDirections from 'react-native-maps-directions';

// import MapboxGL from '@mapbox/react-native-mapbox-gl';
// MapBoxGL token
// MapboxGL.setAccessToken("pk.eyJ1IjoidHJheHgxODYiLCJhIjoiY2p1OWliOWljMjhnZzQ0bnIwZ3QxdTk4eiJ9.MjZT4Ze-viZ_0rdtDZVTFA");

import { Redirect } from 'react-router-native';

import Loading from '../components/Loading';
import MultiFingerMap from '../components/MultiFingerMap';

import config from '../config.json';

class Project extends Component {
	constructor(props) {
		super(props);

		this.state = {
			project: false,
			isReady: false,
			project: null,
			scrollingEnabled: true,
		};

		this.props.loader.show();
		this.props.setHeader({ hidden: true });
		this.props.setFooter({ hidden: true });
		this.getProject();
	}

	onShare() {
		Share.share({
			message: 'Bekijk '+ this.state.project.name +' project op deze link: http://dev.hetvernuft.nl/project/' + this.state.project.slug,
			url: 'http://dev.hetvernuft.nl/project/' + this.state.project.slug,
			title: this.state.project.name
		}, {
			// Android only:
			dialogTitle: this.state.project.name,
		});
	}

	onPlaceClick(msg) {
		Clipboard.setString(
			(
				this.state.project.address === (" " || "") || (null || !this.state.project.address)) ? this.state.project.name : this.state.project.address
    			+
    			", "
    			+
    			this.state.project.project_location.name
    		);
		Toast.show({
			text: "Locatie gekopieerd naar klembord!",
			duration: 3000,
			type: 'success'
		});
	}

	async getProject() {
		try {
			let project = await fetch(config.api.BASE_URL + config.api.PROJECT + this.props.id);
			project = await project.json();
			this.setState({ project: project.data, isReady: true });
			this.props.setHeader({ title: project.data.name, back: true, hidden: false, content: <Button transparent onPress={this.onShare.bind(this)}><Icon style={{fontSize: Platform.OS === "ios" ? 35 : 20}} type={Platform.OS === "ios" ? "EvilIcons" : "Ionicons"} name={Platform.OS === "ios" ? "share-apple" : "share"} /></Button> });
		} catch(e) {
			this.props.setHeader({ title: "Project", back: true, hidden: false });
		} finally {
			this.props.loader.hide();
			this.props.setFooter({ hidden: false, active: 0 });
		}
	}

	componentDidMount() {
		this.getProject();
	}

	onMapRef(ref) {
		if (ref) {
			this._map = ref;
		}
	}

	onMapDrag(event) {
		// event.persist();
			// this._map.on('dragstart', (event) => {
			//     if (event.originalEvent && 'touches' in event.originalEvent && event.originalEvent.touches.length >= 2) {
			//     } else {
			//         this.map.dragPan.disable();
			//         this.map.dragPan.enable();
			//     }
			// });
	}

	onLinkPress(a, b, c, d, e, f) {
	}

	onParsed() {
		if (this.state.isReady !== true) {
			this.setState({ isReady: true });
		}
	}

	_onRefresh = () => {
		this.props.loader.show()
		this.props.setHeader({ hidden: true });
		this.setState({isReady: false});
		this.getProject();
	}

	render() {
		if (!this.state.isReady) {
			return(null)
		}
		if (!this.state.project && this.state.isReady) {
			return(<Redirect to={'/projects'} />);
		}
		return(
			<Content
				style={{ flex: 1, zIndex: 1 }}
				refreshControl={
					<RefreshControl
						refreshing={!this.state.isReady}
						onRefresh={this._onRefresh}
					/>
				}
				scrollEnabled={this.state.scrollingEnabled}
				ref={r => this.content = r}
			>
            	<Image
					resizeMode={'cover'}
					style={{ width: '100%', height: 200 }}
					source={{uri: this.state.project.thumbnail }}
				/>
				<View style={{ marginLeft: 20, marginRight: 30, marginTop: 20, marginBottom: 20, textAlign: 'center' }}>
					<View style={{flex:1, flexDirection: 'row', justifyContent: 'space-between'}}>
						<View style={{ paddingRight: 15 }}>
	            			<H1>{ this.state.project.name }</H1>
							<Text note>{ this.state.project.project_location.name }</Text>
		            		<Text note>{ this.state.project.construction_year }</Text>
						</View>
						<View style={{ flex: 0, marginLeft: -25 }}>
							{ this.state.project.free === 0 ? <Text style={{ color: 'red', fontSize: 14 }}>Betaald</Text> : <Text style={{ color: 'green', fontSize: 14 }}>Gratis</Text> }
						</View>
					</View>
                	<HTML
                		baseFontStyle={{ fontSize: 18, color: "#000" }}

                		html={this.state.project.description}
                		imagesMaxWidth={Dimensions.get('window').width}
                		onLinkPress={this.onLinkPress}
                		textSelectable={true}
                		ignoredTags={["script", "scripts", "style"]}
                		onParsed={this.onParsed.bind(this)}
                	/>
                	<Button full bordered onPress={e => this.onPlaceClick()} style={{ marginTop: 40 }}>
                		<Text style={{ textDecorationLine: 'underline' }}>{(this.state.project.address === (" " || "") || (null || !this.state.project.address)) ? this.state.project.name : this.state.project.address
            			+
            			", "
            			+
            			this.state.project.project_location.name}</Text>
                	</Button>
				</View>
				<View style={{position:'relative'}}>
					<MultiFingerMap
			          provider={PROVIDER_GOOGLE}
			          ref={ref => {
			            this.map = ref;
			          }}
			          style={{ flex: 1, width: '100%', height: Dimensions.get('window').height - 130 - ( Platform.OS === "ios" ? 40 : 0 ) }}
			          initialCamera={{
			            center: {
			              latitude: Number(this.state.project.latitude),
			              longitude: Number(this.state.project.longitude),
			            },
			            pitch: 0,
			            heading: 90,
			            altitude: 1000,
			            zoom: 15,
			          }}
			          showsUserLocation
			        >
			        	<MapView.Marker coordinate={{
			              latitude: Number(this.state.project.latitude),
			              longitude: Number(this.state.project.longitude),
			            }} />
			            
			        </MultiFingerMap>
				</View>
            </Content>
		);
					// <MapboxGL.MapView
					// 	ref={this.onMapRef.bind(this)}
					// 	style={{flex: 1, width: '100%', height: Dimensions.get('window').height - 80}}
					// 	zoomLevel={15}
					// 	centerCoordinate={[Number(this.state.project.longitude), Number(this.state.project.latitude)]}
					// >
			  //       </MapboxGL.MapView>
			   //        <MapboxGL.PointAnnotation
						// title={this.state.project.name}
						// id='0'
						// coordinate={[Number(this.state.project.longitude), Number(this.state.project.latitude)]}>
				  //       	<Icon type="FontAwesome5" name="map-marker" style={{color: '#00627c', fontSize: 34}} />
				  //     </MapboxGL.PointAnnotation>
	}
}
                // <HTML html={this.state.project.description} imagesMaxWidth={Dimensions.get('window').width} onLinkPress={this.onLinkPress} textSelectable={true} ignoredTags={["script", "scripts"]} onParsed={this.onParsed.bind(this)} />

export default Project;