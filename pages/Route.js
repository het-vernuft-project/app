import React, { Component } from 'react';

import { View, Image, Dimensions, Platform } from 'react-native';
import { Link } from 'react-router-native';
import { Button, Container, Content, Text } from 'native-base';

import MapRoute from '../components/MapRoute';

import config from '../config.json';

class Route extends Component {
	constructor(props) {
		super(props);

		this.state = {
			isReady: false,
			route: undefined
		};
		this.getRoute();
		this.props.setHeader({ hidden: true });
		this.props.setFooter({ hidden: true });
	}

	async getRoute() {
		try {
			let route = await fetch(config.api.BASE_URL + config.api.ROUTE + this.props.id);
			route = await route.json();
			this.setState({ route: route.data, isReady: true });
			this.props.setHeader({ hidden: false, title: route.data.name, back: true,
				content: (<Button transparent onPress={e => this.mapRoute.startNavigation()}><Text style={{ marginTop: Platform.OS === "ios" ? 0 : 10 }}>Start route</Text></Button>)
			});
		} catch(e) {
			this.props.setHeader({ title: "Route", back: true, hidden: false });
		} finally {
			this.props.loader.hide();
			this.props.setFooter({ hidden: false, active: 1 });
		}
	}

	render() {
		if (this.state.isReady === false) {
			return(null);
		}
		if (this.state.isReady === true && !this.state.route) {
			return(
				<Text>Could not load route!</Text>
			);
		}
		return(
			<Content>
				<MapRoute 
					style={{ flex: 1, width: '100%', height: Dimensions.get('window').height - 130 - ( Platform.OS === "ios" ? -10 : 5 ) }}
					apiKey="AIzaSyA0EMKjAKiR8BmuaEpqKbQFRg5vIgemqR0"

					// locations={[
					// 	{ location: { latitude: 52.347025, longitude: 4.906041 }, name: "Project 1" },
					// 	{ location: { latitude: 52.406164, longitude: 4.757929 }, name: "Project 2" },
					// 	{ location: { latitude: 52.399946, longitude: 4.804941 }, name: "Project 3" },
					// ]}
					locations={this.state.route.locations}

					ref={ref => this.mapRoute = ref}
					pushPath={this.props.pushPath}
				/>
			</Content>
		);
	}
}

export default Route;