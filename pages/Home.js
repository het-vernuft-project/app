import React, { Component } from 'react';

import { View, Text, Image } from 'react-native';
import { Link } from 'react-router-native';
import { Button, Container, Content, H2 } from 'native-base';

import { getItem, setItem } from '../utils';

import Logo from '../images/logo.png';

class Home extends Component {
	constructor(props) {
		super(props);

		this.state = {

		};

		this.props.setHeader({ hidden: true });
		this.props.setFooter({ hidden: true });
	}

	onBtnPress() {
		setItem('@store:path', null);
		this.props.replacePath('/projects');
	}

	componentDidMount() {
		this.props.loader.hide();
	}

	render() {
		return(
			<Content>
				<View style={{ height: 50, width: '100%', backgroundColor: '#e94a34' }}>
					<Image
						style={{ height: 45, width: 'auto', resizeMode: 'contain' }}
						source={Logo}
					/>
				</View>
				<View style={{ width: '100%', padding: 20 }}>
					<Text style={{ color: '#000', fontSize: 18 }}>
						NLingenieurs is de Nederlandse branchevereniging van advies-, management- en ingenieursbureaus. De leden van NLingenieurs zijn private bedrijven, variërend van kleine bureaus tot wereldwijd opererende ondernemingen, die kwalitatief hoogwaardige ingenieursdiensten leveren, op basis van vrije concurrentie.
					</Text>
					<Text style={{ color: '#000', fontSize: 18 }}>
						Deze webapp toont bijzondere projecten van bij NLingenieurs aangesloten bureaus, waarbij het vernuft van de projecten uitgelicht wordt. Wat is er zo speciaal aan een gebouw, brug, sluis, fiets- of tramverbinding, et cetera?
					</Text>

					<View style={{ width: '100%', padding: 25, paddingLeft: 30, paddingRight: 30, backgroundColor: '#e94a34', textAlign: 'center', marginTop: 40 }}>
						<H2 style={{ textAlign: 'center', color: '#fff', marginBottom: 10 }}>Bekijk alle projecten</H2>
						<Button style={{ backgroundColor: '#00627e' }} block onPress={this.onBtnPress.bind(this)}>
							<Text style={{ color: '#fff', fontSize: 18 }}>Naar projecten</Text>
						</Button>
					</View>
				</View>
			</Content>
		);
	}
}

export default Home;