import React, { Component } from 'react';

import { Image, ScrollView, Text, RefreshControl, TouchableOpacity, View, Dimensions, Platform, FlatList } from 'react-native';

import { Button, Icon, Content, Form, Item, Picker, Label, Input } from 'native-base';

import Project from '../components/Project';
import Loading from '../components/Loading';
import ProjectsFilter from '../components/filters/ProjectsFilter';

class Projects extends Component {
  constructor(props) {
    super(props);

    this.state = {
      projects: [
        // {
        //   title: "De Rijtuigenloods – NLingenieurs jubileum",
        //   constructionYear: 1904,
        //   place: "Amersfoort",
        //   image: "https://app.nlingenieurs.nl/wp-content/uploads/2017/10/rijtuigenloods009-e1509372494384.jpg",
        // },
        // {
        //   title: "Maeslantkering",
        //   constructionYear: 1995,
        //   place: "Rotterdam",
        //   image: "https://app.nlingenieurs.nl/wp-content/uploads/2017/06/Maeslantkering-Foto-1280x640_tcm21-19633.png",
        // },
        // {
        //   title: "Stadhuiskwartier Deventer",
        //   constructionYear: 2014,
        //   place: "Deventer",
        //   image: "https://app.nlingenieurs.nl/wp-content/uploads/2017/05/DGMR-Project-Stadhuiskwartier-Deventer.jpg",
        // }
      ],
      isReady: true,
      tempProjects: this.props.projects,
      search: "",
      noSearchResults: false,
      page: 1,
      filterActive: false,
      filters: { 
        sortBy: 'alfabet',
        constructionYear: {
          from: undefined,
          till: undefined },
        reversed: false,
      },
    };

    // this.props.setHeader({ hidden: true });
    // this.props.setFooter({ hidden: true });
    // this.getProjects();
  }

  handleSearch(search) {
    this.searchProjects(search);
  }

  componentDidMount() {
    this.props.loader.hide();
    this.showNavigations();
    this.searchProjects();
  }

  searchProjects(search) {
    let projects = [];
    if (search === "" || search === undefined) {
      projects = this.filter(this.state.filters, this.state.tempProjects);
      this.setState({ projects, search, noSearchResults: false });
    } else {
      this.state.tempProjects.forEach(item => {
        // Delete items that won't be searched
        delete item.description;

        let values = Object.values(item);
        let d = false;
        values.forEach(itm => {
          if (itm.toString().toLowerCase().search(search.toLowerCase()) !== -1 && d === false) {
            d=true;
            projects.push(item);
          }
        });
        if (projects.length === 0) {
          this.setState({ projects: [], noSearchResults: true, page: 1 });
        } else {
          projects = this.filter(this.state.filters, projects);
          this.setState({ projects, search, noSearchResults: false, page: 1 });
        }
      });

    }
  }

  sortBy(type, reversed, projects) {
    if (!type || type === 'alfabet') {
      projects = projects.sort((a,b)=> (a.name>b.name)*2-1);
    } else if (type === 'nieuwtoegevoegd') {
      projects = projects.sort((a,b)=> (a.created_at>b.created_at)*2-1);
    } else if (type === 'constructiejaar') {
      projects = projects.sort((a,b)=> (a.construction_year>b.construction_year)*2-1);
    }
    if (reversed) {
      return projects.reverse();
    }
    return projects;
  }

  filter(filters, projects) {
    if (!filters || !filters.constructionYear) return projects;
    if (filters.constructionYear.from != undefined && filters.constructionYear.till != undefined) {
      let from = Number(filters.constructionYear.from);
      let till = Number(filters.constructionYear.till);
      projects = projects.filter(project => {
        let cy = project.construction_year;
        if (cy >= from && cy <= till) {
          return true;
        }
        return false;
      });
    }
    projects = this.sortBy(filters.sortBy, filters.reversed, projects);
    return projects;
  }

  onFilterBtnPress(filters) {
    if (filters.sortBy == undefined && (filters.constructionYear.from == undefined || filters.constructionYear.till == undefined)) { // No filters are active
      filters = null;
    }
    this.setState({ filterActive: false, filters }, () => {
      this.searchProjects(this.state.search);
    });
  }

  onFilterPress() {
    this.setState({ filterActive: !this.state.filterActive });
    // Remove searchbar
    this.props.setHeader({ title: "Projecten", back: false, hidden: false, search: false, onSearch: this.handleSearch.bind(this), onFilterPress: this.onFilterPress.bind(this) });
    // Add searchbar so it is inactive
    this.props.setHeader({ title: "Projecten", back: false, hidden: false, search: true, onSearch: this.handleSearch.bind(this), onFilterPress: this.onFilterPress.bind(this) });
  }

  showNavigations() {
    this.props.setHeader({ title: "Projecten", back: false, hidden: false, search: true, onSearch: this.handleSearch.bind(this), onFilterPress: this.onFilterPress.bind(this) });
    this.props.setFooter({ hidden: false, active: 'Projecten' });
  }

  // async getProjects() {
  //   try {
  //     let projects = await fetch('http://api.dev.hetvernuft.nl/projects', { headers: { 'Accept': 'application/json' } });
  //     if (projects.status !== 404) {
  //       projects = await projects.json();
  //       this.setState({ tempProjects: projects.data });
  //     }
  //   } catch(e) {
  //     this.setState({ tempProjects: [] });
  //   } finally {
  //     this.showNavigations();
  //     this.props.loader.hide();
  //     this.setState({ isReady: true });
  //     this.searchProjects();
  //   }
  // }

  nextPage() {
    this.setState({ page: this.state.page+1 });
  }

  _onRefresh = () => {
    this.props.reloadProjects();
    this.props.loader.show();
    this.props.setHeader({ hidden: true });
    this.props.setFooter({ hidden: true });
    this.setState({isReady: false});
  }

  componentWillReceiveProps(props) {
    if (this.state.tempProjects !== props.projects && this.state.isReady === false) {
      this.props.loader.hide();
      this.showNavigations();
      this.setState({ tempProjects: props.projects, isReady: true });
    } else if(this.state.isReady === false) {
      this.props.loader.hide();
      this.setState({ isReady: true });
    }
  }

  render() {
    const {height: screenHeight} = Dimensions.get('window');
    // if (!this.state.isReady) {
    // }
    if (this.state.projects.length === 0 && this.state.isReady && this.state.noSearchResults === false) { // Could not load projects from API.
      return(
        <Content
          style={{ paddingLeft: 5, paddingRight: 5 }}
          refreshControl={
            <RefreshControl
              refreshing={!this.state.isReady}
              onRefresh={this._onRefresh}
            />
          }
        >
          <View
            style={{
              height: screenHeight,
              flex: 1,
              alignSelf:'center',
              justifyContent: 'center',
              marginTop: Platform.OS === "ios" ? -30 : -50,
            }}
          >
            
            <Text style={{fontSize: 22}}>Kon de projecten niet laden</Text>
            <View style={{alignSelf: 'center', marginTop: 5}}>
              <Button rounded onPress={this._onRefresh.bind(this)} style={{ width: 45, height: 45, padding: 0, margin: 0 }}>
                <Icon style={{width: '100%', marginLeft: 0, marginRight: 0, textAlign: 'center'}} name="refresh"/>
              </Button>
            </View>
          </View>
        </Content>
      );
    }
    if (this.state.filterActive === true) {
      return(
        <ProjectsFilter
          onCancelBtnPress={e => this.setState({ filterActive: false })}
          onFilterBtnPress={filters => this.onFilterBtnPress(filters)}
          {
            ...this.state.filters
          }
        />
      );
    }
    if (this.state.projects.length === 0 && this.state.isReady && this.state.noSearchResults === true) { // Could not find projects from search/filter
      return(
        <Content
          style={{ paddingLeft: 5, paddingRight: 5 }}
          refreshControl={
            <RefreshControl
              refreshing={!this.state.isReady}
              onRefresh={this._onRefresh}
            />
          }
        >
          <View
            style={{
              height: screenHeight,
              flex: 1,
              alignSelf:'center',
              justifyContent: 'center',
              marginTop: Platform.OS === "ios" ? -100 : -120,
            }}
          >
            
            <Text style={{fontSize: 22}}>Geen zoekresultaten gevonden!</Text>
          </View>
        </Content>
      );
    }
    return( // Show projects
      <FlatList
        refreshControl={
          <RefreshControl
            refreshing={!this.state.isReady}
            onRefresh={this._onRefresh}
          />
        }
        data={this.state.projects.slice(0, 10*this.state.page)}
        keyExtractor={item => item.id.toString()}
        renderItem={({ item }) => (
          <Project {...item} pushPath={this.props.pushPath} />
        )}
        onEndReached={this.nextPage.bind(this)}
        onEndReachedThreshold={0.5}

        ListHeaderComponent={
          <Button
            transparent
            full
            style={{ marginBottom: -5 }}
            onPress={e => this.onFilterPress()}
          >
            <Text>Open filters</Text>
          </Button>
        }
      />
    );
  }
}

export default Projects;