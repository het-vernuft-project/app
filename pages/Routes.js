import React, { Component } from 'react';

import { Image, View, TouchableOpacity, RefreshControl } from 'react-native';

import { Content, Text, H1 } from 'native-base';

import config from '../config.json';

class Routes extends Component {
	constructor(props) {
		super(props);

		this.state = {
			routes: [
				
			],
			isReady: false
		};

		this.props.setHeader({ hidden: true });
		this.props.setFooter({ hidden: true, active: 1 });

	}

	showNavigations() {
		this.props.setHeader({ hidden: false, title: "Routes" });
		this.props.setFooter({ hidden: false, active: 1 });
	}

	componentDidMount() {
		this.getRoutes();
	}

	async getRoutes() {
		try {
	      let routes = await fetch(config.api.BASE_URL + config.api.ROUTES, { headers: { 'Accept': 'application/json' } });
	      if (routes.status !== 404) {
	        routes = await routes.json();
	        this.setState({ routes: routes.data });
	      }
	    } catch(e) {
	      this.setState({ routes: [] });
	    } finally {
	      this.showNavigations();
	      this.props.loader.hide();
	      this.setState({ isReady: true });
	    }
		// this.props.setHeader({ hidden: false, title: "Routes" });
		// this.props.setFooter({ hidden: false, active: 1 });
		// this.setState({ isReady: true, routes: [
		// 	{ title: "Test Route", projects: 4, created_on: "17/04/2019", city: "Amsterdam" },
		// 	{ title: "Test Route 2", projects: 6, created_on: "17/04/2019", city: "Amsterdam" },
		// 	{ title: "Test Route 3", projects: 2, created_on: "17/04/2019", city: "Zaandam" },
		// 	{ title: "Test Route 4", projects: 12, created_on: "17/04/2019", city: "Dordrecht" }
		// ] });
	}

	renderRoutes(item, i) {
		d = new Date(item.created_at);
		return(
			<TouchableOpacity key={item.title + item.id} onPress={e => this.props.pushPath('/routes/' + item.id)}>
				<View style={{ flex: 1, padding: 25, backgroundColor: '#f0f4f6', margin: 15 }}>
					<H1>{ item.name }</H1>
					<Text note>{ item.projects_count } projecten</Text>
					<Text>{ item.city || "" }</Text>
					<Text style={{ height: 5 }} />
					<Text>Gemaakt op: <Text note>{ d.getDate() + "-" + (d.getMonth()+1) + "-" + d.getFullYear() }</Text></Text>
				</View>
			</TouchableOpacity>
		);
	}

	_onRefresh() {
		this.getRoutes();
	}

	render() {
		return(
			<Content
				refreshControl={
		          <RefreshControl
		            refreshing={!this.state.isReady}
		            onRefresh={this._onRefresh.bind(this)}
		          />
		        }
			>
				{ this.state.routes.map(this.renderRoutes.bind(this)) }
			</Content>
		);
	}
}

export default Routes;