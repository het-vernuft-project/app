import React, { Component } from 'react';

import { View, TextInput } from 'react-native';
import { Form, Item, Input, Picker, Label, Icon, Text, Button } from 'native-base';

class ProjectsFilter extends Component {
	constructor(props) {
		super(props);

		this.state = {
			sortBy: this.props.sortBy || 'alfabet',
			constructionYear: {
				from: (this.props.constructionYear && this.props.constructionYear.from) || undefined,
				till: (this.props.constructionYear && this.props.constructionYear.till) || undefined },
			reversed: this.props.reversed || false,
		};
	}

	onConstructionYearChange(type, value) {
		if (type === 'from' && this.isNum(value)) {
			this.setState({ constructionYear: { from: value, till: this.state.constructionYear.till } })
		} else if (type === 'till' && this.isNum(value)) {
			this.setState({ constructionYear: { from: this.state.constructionYear.from, till: value } })
		}
	}

	isNum(n) {
		return !isNaN(Number(n))
	}

	onFilterBtnPress(e, reset) {
		if (reset === true) {
			this.props.onFilterBtnPress({ 
				sortBy: 'alfabet',
				constructionYear: {
					from: undefined,
					till: undefined
				},
				reversed: false,
			});
			return;
		}
		let { sortBy, constructionYear, reversed } = this.state;
		this.props.onFilterBtnPress({ sortBy, constructionYear, reversed });
	}

	render() {
		return(
			<View style={{ height: '100%' }}>
				<View>
	              	<Text>Bouwjaar</Text>
	              	<View style={{ flexDirection: 'row', flexWrap: 'wrap' }}>
		      			<TextInput
		      				style={{ width: '49%', borderWidth: 1, borderColor: '#cecece', borderRadius: 5, marginLeft: '0.5%', paddingLeft: 10, paddingRight: 10, paddingTop: 5, paddingBottom: 5 }}
			              	keyboardType="numeric"
			              	placeholder="0"
			              	onChangeText={text => this.onConstructionYearChange('from', text)}
			              	value={this.state.constructionYear.from}
			              	placeholderTextColor="#cecece"
			            />
			            <TextInput
		      				style={{ width: '49%', borderWidth: 1, borderColor: '#cecece', borderRadius: 5, marginLeft: '0.5%', paddingLeft: 10, paddingRight: 10, paddingTop: 5, paddingBottom: 5 }}
							keyboardType="numeric"
							placeholder={(new Date()).getFullYear().toString()}
			              	onChangeText={text => this.onConstructionYearChange('till', text)}
			              	value={this.state.constructionYear.till}
							placeholderTextColor="#cecece"
						/>
	              	</View>
					<Text>Sorteren op</Text>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ borderWidth: 1, borderColor: '#cecece', borderRadius: 5 }}
						placeholderIconColor="#007aff"
						selectedValue={this.state.sortBy}
						onValueChange={val => this.setState({ sortBy: val })}
					>
						<Picker.Item label="Alfabet" value="alfabet" />
						<Picker.Item label="Nieuw toegevoegd" value="nieuwtoegevoegd" />
						<Picker.Item label="Constructiejaar" value="constructiejaar" />
					</Picker>
					<Picker
						mode="dropdown"
						iosIcon={<Icon name="arrow-down" />}
						style={{ borderWidth: 1, borderColor: '#cecece', borderRadius: 5 }}
						placeholderIconColor="#007aff"
						selectedValue={this.state.reversed}
						onValueChange={val => this.setState({ reversed: val })}
					>
						<Picker.Item label="Oplopend" value={false} />
						<Picker.Item label="Aflopend" value={true} />
					</Picker>
		              <View style={{ flexDirection: 'row', flexWrap: 'wrap', alignItems: 'center', justifyContent: 'space-around', marginTop: 50, zIndex: 0 }}>
		              	<Button style={{ flex: 0 }} onPress={this.props.onCancelBtnPress}><Text>Annuleren</Text></Button>
		              	<Button style={{ flex: 0 }} onPress={this.onFilterBtnPress.bind(this)}><Text>Filter</Text></Button>
		              </View>
				</View>
              	<Button style={{ position: 'absolute', right: 20, bottom: 20 }} onPress={e => this.onFilterBtnPress(e, true)}><Text>Reset</Text></Button>
	        </View>
		);
	}
}

export default ProjectsFilter;