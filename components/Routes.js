import React, { Component } from 'react';

import { BackHandler } from 'react-native';
import { Text } from 'native-base';
import { withRouter, Route, Switch, BackButton } from 'react-router-native';

import { getItem, setItem } from '../utils';

import RoutesPage from '../pages/Routes';
import RoutePage from '../pages/Route';
import ProjectsPage from '../pages/Projects';
import ProjectPage from '../pages/Project';
import HomePage from '../pages/Home';

let interval;

class Routes extends Component {
  constructor(props) {
    super(props);
    this.state = {};
    if (typeof this.props.path === "object") {
      this.props.history.entries = this.props.path;
      this.props.history.go(this.props.path.length-1);
    }

    this.pushPath = this.pushPath.bind(this);
    this.replacePath = this.replacePath.bind(this);
    this.backPress = this.backPress.bind(this);
    this.listener = this.listener.bind(this);
    this.props.onRouteProps({...this.props, backPress: this.backPress, replacePath: this.replacePath, pushPath: this.pushPath});
  }

  componentDidMount() {
	this.props.history.listen(this.listener);
    BackHandler.addEventListener('hardwareBackPress', () => {
      this.props.history.entries.pop();
    });
  }

  replacePath(path) {
    this.props.history.replace(path);
  }

  pushPath(path) {
    this.props.history.push(path);
  }

  listener(c, location) {
    setItem('@store:path', this.props.history.entries);
  }

  backPress() {
    this.props.history.entries.pop();
    this.props.history.goBack(null);
  }

  render() {
    return(
      <BackButton>
        <Switch>
        	{ /* All the routes of the app where the user can navigate to. Otherwise called all of the screens */ }
          <Route exact path="/" render={() => {
          	// Only show introduction once. Or when the user wants to see it again
            return(
            	<HomePage
            		replacePath={this.replacePath}
            		setHeader={this.props.setHeader}
            		setFooter={this.props.setFooter}
            		loader={this.props.loader}
        		/>
    		);
          }} />
          <Route exact path="/projects" render={() => {
          	// Route for the projects path
            return(
            	<ProjectsPage
            		pushPath={this.pushPath}
            		setHeader={this.props.setHeader}
            		setFooter={this.props.setFooter}
            		loader={this.props.loader}
            		projects={this.props.projects}
            		reloadProjects={this.props.reloadProjects}
        		/>
        	);
          }} />
          <Route path="/projects/:id" render={(match) => {
          	// Route for the specified project
            return(<ProjectPage
	            	id={match.match.params.id}
	            	setHeader={this.props.setHeader}
	            	setFooter={this.props.setFooter}
	            	loader={this.props.loader}
            	/>
            );
          }} />
          <Route exact path="/routes" render={() => {
          	// Route for the routes path
            return(
            	<RoutesPage
            		pushPath={this.pushPath}
            		setHeader={this.props.setHeader}
            		setFooter={this.props.setFooter}
            		loader={this.props.loader}
        		/>
            );
          }} />
          <Route path="/routes/:id" render={(match) => {
          	// Route for the specified route
            return(<RoutePage
            		id={match.match.params.id}
            		pushPath={this.pushPath}
            		setHeader={this.props.setHeader}
            		setFooter={this.props.setFooter}
            		loader={this.props.loader}
        		/>
            );
          }} />
        </Switch>
      </BackButton>
    );
  }
}

export default withRouter(Routes);