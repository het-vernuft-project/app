import React, { Component } from 'react';
import { Dimensions, Platform, Animated } from 'react-native';

import { Button, Text, View, Spinner } from 'native-base';

// import * as Animatable from 'react-native-animatable';

import { delayUnmounting } from '../utils';

class Loading extends Component {
	constructor(props) {
		super(props);

		this.state = {
			hidden: false,
			fade: new Animated.Value(1)
		};

		this.show = this.show.bind(this);
		this.hide = this.hide.bind(this);
	}

	show() {
		if (this.state.hidden === true) {
			this.setState({ hidden: false, fade: new Animated.Value(1) }, () => {});

		}
		// if (this.state.hidden === true) {
		// } else {
		// 	this.props.onShowAnimationEnd ? this.props.onShowAnimationEnd() : null
		// }
	}

	hide() {
		if (this.state.hidden === false) {
			this.setState({ hidden: true }, () => {
				Animated.timing(this.state.fade, {
					toValue: 0.01,
					duration: 1000,
					useNativeDriver: true,
				}).start()
			});
		}
		// if (this.state.hidden === false) {
		// 		this.setState({ hidden: true }, () => {
		// 			this.view.animate('fadeOut', 1000).then(endState => {
		// 				return endState;
		// 			}).then(this.props.onHideAnimationEnd ? this.props.onHideAnimationEnd : () => {});
		// 		});
		// } else {
		// 	this.props.onHideAnimationEnd ? this.props.onHideAnimationEnd() : null
		// }
	}

	// componentWillReceiveProps(p) {
	// 	if (p.animate === true) {
	// 		this.view.animate('fadeOut', 500).then(this.props.onAnimationEnd ? this.props.onAnimationEnd : () => {});
	// 	}
	// }

	handleViewRef = ref => this.view = ref;

	render() {
		return(null);
		let { fade } = this.state;
		var {height, width} = Dimensions.get('window');
		if (Platform.OS === "android") {
			height = height + 50;
		}

		return(
			<Animated.View
				style={{ width: width, height: height, backgroundColor: '#e84934', position: 'absolute', zIndex: 2, top: 0, left: 0, opacity: fade }}
				// ref={this.handleViewRef}
				useNativeDriver
				{...this.state.hidden ? {pointerEvents: 'none'} : {}}
			>
				<View
					style={{
						flex: 1,
					    justifyContent: 'center',
					    alignItems: 'center'
					}}
				>
					<Spinner
						style={{ marginTop: -50, transform: [{scale: 2}] }}
						color="white"
						// size={50} // CANNOT BE DONE IN IOS
					/>
					<Text style={{ color: 'white' }}>
						Loading
					</Text>
				</View>
			</Animated.View>
		);
	}
}

export default Loading;