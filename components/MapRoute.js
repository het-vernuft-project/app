import React, { Component } from 'react';

import MapView from 'react-native-maps';

import { View, TouchableOpacity, Dimensions, Linking, Text, Platform } from 'react-native';

import { Fab, Button, Icon } from 'native-base';

import { decodePolylines, swapArray } from '../utils';
import Polyline from '@mapbox/polyline';

class MapRoute extends Component {
	constructor(props) {
		super(props);

		this.state = {
			nativeResponse: null,
			points: null,
			route: null,
			mode: "DRIVING",
			fabActive: false,
			fabs: [
				{ mode: 'driving', icon: 'car' },
				{ mode: 'bicycling', icon: 'bicycle' },
				{ mode: 'walking', icon: 'walking' },
			]
		};

		if (this.props.locations.length > 1) this.getDirections(this.state.mode);
	}

	getStrLoc(location) {
		return location.latitude + "," + location.longitude;
	}

	getStrArrLoc(locations) {
		locs = [];

		locs = locations.map((item, i) => {
			return this.getStrLoc(item) + (locations.length-1 != i ? "|" : "");
		});

		return locs.join("");
	}

	shouldComponentUpdate(props, state) {
		if (this.state.mode !== state.mode) {
			this.getDirections(state.mode);
			return false;
		}
		return true;
	}

	async getDirections(travelMode) {
		const base = 'https://maps.googleapis.com/maps/api/directions/json';
		const origin = `?origin=${ this.getStrLoc(this.props.locations[0]) }`;
		const waypoints = `&waypoints=${ this.getStrArrLoc(this.props.locations.slice(1, this.props.locations.length-1)) }`;
		const destination = `&destination=${ this.getStrLoc(this.props.locations[this.props.locations.length-1]) }`;
		const apiKey = `&key=${ this.props.apiKey }`;
		const mode = `&mode=${ travelMode }`;
		const url = base + origin + destination + waypoints + apiKey + mode;

        try {
            let resp = await fetch(url);
            let respJson = await resp.json();
            if (!respJson.routes[0].legs) return null;
            let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
	        let coords = points.map((point, index) => {
	            return  {
	                latitude : point[0],
	                longitude : point[1]
	            }
	        })
            this.setState({
            	// nativeResponse: respJson,
            	// points: coords,
            	route: (<MapView.Polyline
					coordinates={coords}
					strokeWidth={ this.props.strokeWidth || 2 }
					strokeColor={ this.props.strokeColor || "#0088FF" }
				/>),
            });
        } catch(error) {
        	return null;
        }
    }

    startNavigation() {
    	const base = 'https://www.google.com/maps/dir/?api=1&dir_action=navigate';
		const waypoints = `&waypoints=${ this.getStrArrLoc(this.props.locations.slice(0, this.props.locations.length-1)) }`;
		const destination = `&destination=${ this.getStrLoc(this.props.locations[this.props.locations.length-1]) }`;
		const apiKey = `&key=${ this.props.apiKey }`;
		const mode = `&travelmode=${ this.state.mode.toLowerCase() }`;
		const url = base + destination + waypoints + apiKey + mode;
    	// var url = "https://www.google.com/maps/dir/?api=1&travelmode=driving&dir_action=navigate&destination=Los+Angeles";
		Linking.canOpenURL(url).then(supported => {
		    if (!supported) {
		    } else {
		        return Linking.openURL(url);
		    }
		}).catch(err => console.error('An error occurred', err));
    }

    renderNavigationButton() {
		if (Platform.OS === "ios") return(null);
    	return(
    		<View style={{ zIndex: 1, position: 'absolute', bottom: 25, right: 25 }}>
				<View style={{ flex: 1, flexDirection: 'row' }}>
					<TouchableOpacity onPress={this.startNavigation.bind(this)}>
						<View style={{ height: 50, width: 50, backgroundColor: '#fff', borderWidth: 1, borderColor: 'grey', justifyContent: 'center', alignItems: 'center' }}>
							<Icon type="FontAwesome5" name="route" />
						</View>
					</TouchableOpacity>
				</View>
			</View>
    	);
    }

    renderFabButtons() {
    	if (!this.state.fabActive) {
    		return(
				<View style={{ zIndex: 1, position: 'absolute', bottom: 25, left: 25 }}>
					<View style={{ flex: 1, flexDirection: 'row' }}>
						<TouchableOpacity onPress={e => this.setState({ fabActive: !this.state.fabActive })}>
							<View style={{ height: 50, width: 50, backgroundColor: '#fff', borderWidth: 1, borderColor: 'grey', justifyContent: 'center', alignItems: 'center' }}>
								<Icon type="FontAwesome5" name={this.state.fabs[0].icon} />
							</View>
						</TouchableOpacity>
					</View>
				</View>
    		);
    	} else {
    		return(
    			<View style={{ zIndex: 1, position: 'absolute', bottom: 25, left: 25 }}>
					<View style={{ flex: 1, flexDirection: 'row' }}>
						{
							this.state.fabs.map((item, i) => {
								return(
									<TouchableOpacity onPress={e => this.setState({ fabActive: !this.state.fabActive, mode: item.mode, fabs: swapArray(this.state.fabs, 0, i) })}>
										<View style={{ height: 50, width: 50, backgroundColor: '#fff', borderWidth: 1, borderColor: 'grey', justifyContent: 'center', alignItems: 'center' }}>
											<Icon type="FontAwesome5" name={item.icon} {...(i === 0 ? {style: {color: '#a8a8a8'}} : {})} />
										</View>
									</TouchableOpacity>
								);
							})
						}
					</View>
				</View>
    		);
    	}
    }

    renderMarker(item, i) {
    	return(
    		<MapView.Marker key={"marker"+i}
	    		coordinate={{
	              latitude: Number(item.latitude),
	              longitude: Number(item.longitude),
	            }}
	            title={ item.project_name || "Project " + (i+1) }
	            onCalloutPress={e => this.props.pushPath('/projects/' + item.project_id)}
            >
				<View style={{
					position: 'relative',
					width: 30,
					height: 30,
					borderRadius: 30 / 2,
					backgroundColor: 'red',
				}}>
					<Text style={{
						color: 'white',
					    fontWeight: 'bold',
					    textAlign: 'center',
					    fontSize: 20,
					    marginTop: 2.5
					}}>{i+1}</Text>
				</View>
            </MapView.Marker>
    	);
    }

	render() {
		return(
			<View>
				{ this.renderFabButtons() }
				<MapView
					ref={ref => {
						this.map = ref;
					}}
					style={{ flex: 1, width: '100%', height: Dimensions.get("window").height }}
					initialCamera={{
						center: { latitude: Number(this.props.locations[0].latitude), longitude: Number(this.props.locations[0].longitude) },
						pitch: 0,
						heading: 0,
						altitude: 1000,
						zoom: 10,
					}}
					{...this.props}
					geodesic
					lineJoin="round"
				>
					{ this.state.route }
					{
						this.props.locations
						?
						this.props.locations.map(this.renderMarker.bind(this))
						:
						null
					}
				</MapView>
			</View>
		);
	}
}

export default MapRoute;