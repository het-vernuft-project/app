import React, { Component } from 'react';

import MapView from 'react-native-maps';

import { View, PanResponder } from 'react-native';

class MultiFingerMap extends Component {
	constructor(props) {
		super(props);

		this.state = {
			allowed: false,
		};
	}

	handleNumberOfTouches(touches) {
		if (touches >= 2 && this.state.allowed === false) {
			this.setState({ allowed: true });
			this.props.onDisableScroll(false);
		} else if (touches <= 1 && this.state.allowed === true) {
			this.setState({ allowed: false });
			this.props.onDisableScroll(true);
		}
	}

	componentWillMount() {
		this._panResponder = PanResponder.create({
				// Ask to be the responder:
				onStartShouldSetPanResponder: (evt, gestureState) => {
				    return false;
				},
				onStartShouldSetPanResponderCapture: (evt, gestureState) => {
				    return false;
				},

				onMoveShouldSetPanResponder: (evt, gestureState) => {
					this.handleNumberOfTouches(gestureState.numberActiveTouches);
				    // Listen for your events and show UI feedback here
				    return false;
				},
				onMoveShouldSetPanResponderCapture: (evt, gestureState) => {
					this.handleNumberOfTouches(gestureState.numberActiveTouches);
				    return true;
				},
				onPanResponderGrant: (evt, gestureState) => {
				   return false;
				},
				onPanResponderMove: (evt, gestureState) => {
				    return false;
				},
				onPanResponderTerminationRequest: (evt, gestureState) => {
				    return false
				},
				onPanResponderRelease: (evt, gestureState) => {
					this.handleNumberOfTouches(gestureState.numberActiveTouches);
				    // This wont get called
				    return true;
				},
				onPanResponderTerminate: (evt, gestureState) => {
				    return false;
				},
				onShouldBlockNativeResponder: (evt, gestureState) => {
					return false;
				},
		});
	}

	handlePress(ev, b) {
		ev.persist();
	}

	render() {
		let { children } = this.props;
		return(
			<View
				style={ this.props.style }
				{...this._panResponder.panHandlers}
			>
				<MapView
					{ ...(
						(
							delete this.props.children
							&&
							delete this.props.pitchEnabled
							&&
							delete this.props.scrollEnabled
							&&
							delete this.props.zoomEnabled
						)
						? this.props : this.props) }
					pitchEnabled={false}
					scrollEnabled={this.state.allowed}
					zoomEnabled={this.state.allowed}
				>{ children }</MapView>
			</View>
		);
	}
}

export default MultiFingerMap;