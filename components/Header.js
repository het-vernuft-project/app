import React, { Component } from 'react';

import { Header, Title, Left, Right, Body, Button, Text, Icon, Input, Item } from 'native-base';

import { Platform } from 'react-native';

import AsyncStorage from '../utils';

class Head extends Component {
	constructor(props) {
		super(props);

		this.state = {
			searching: false,
			searchValue: "",
		};
	}

	componentDidMount() {
	}

	componentDidUpdate() {

	}

	componentWillReceiveProps(props) {
		if (this.state.searching === true && !this.props.search) {
			this.setState({ searching: false });
		}
	}

	onChangeText(value) {
		this.setState({ searchValue: value });
		this.props.options.onSearch ? this.props.options.onSearch(value) : (() => {})()
	}

	render() {
		if (this.state.searching && this.props.options.search === true) {
			return(
				<Header
					style={{ ...(this.props.options.hidden === true ? { display: 'none' } : {  }) }}
					searchBar
				>
					<Item>
						<Icon name="close" onPress={e => this.setState({ searching: false })} />
						<Input
							style={{ flex: 1, width: '100%', height: '100%', paddingLeft: 5, paddingRight: 5 }}
							{ ...this.props.options.hidden === true ? {} : {autoFocus: true} }
							onChangeText={ this.onChangeText.bind(this) }
							placeholder="Search"
							value={this.state.searchValue}
						/>
						<Button transparent dark onPress={this.props.options.onFilterPress}>
							<Text style={{ marginBottom: Platform.OS === "ios" ? 10 : 5 }}>open filters</Text>
						</Button>
					</Item>
				</Header>
			);
		}
		return(
			<Header style={{ ...(this.props.options.hidden === true ? { display: 'none' } : {  }) }}>
				<Left style={{ flex: Platform.OS === "ios" ? 1 : 0 }}>
					{ this.props.options.back === true ? <Button transparent onPress={this.props.backPress}><Icon name="arrow-back" /></Button> : null }
				</Left>
				<Body numberOfLines={1} style={{
					textAlign: Platform.OS === "ios" ? 'center' : 'left',
					flexWrap: 'wrap',
					marginLeft: Platform.OS === "ios" ? 0 : 10,
					marginRight: Platform.OS === "ios" ? 0 : 10
				}}>
					<Title>{ this.props.options.title }</Title>
				</Body>
				<Right style={{ flex: Platform.OS === "ios" ? 1 : 0 }}>
					{this.props.options.content}
					{ this.props.options.search === true ? <Icon type="FontAwesome5" name="search" style={{ color: "#FFF", fontSize: 18 }} onPress={e => this.setState({ searching: true })} /> : null }
				</Right>
			</Header>
		);
	}
}

export default Head;