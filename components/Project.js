import React, { Component } from 'react';

import { Image, TouchableOpacity, View } from 'react-native';

import { Card, CardItem, Left, Body, Text, Button } from 'native-base';

class Project extends Component {
	onPress() {
		this.props.pushPath('/projects/' + this.props.id);
	}

	render() {
		return(
			<TouchableOpacity activeOpacity={.7} onPress={this.onPress.bind(this)}>
				<View style={{ flex: 1, flexDirection: 'row', margin: 15, marginTop: 7.5, marginBottom: 7.5, marginRight: 10, backgroundColor: '#f0f4f6' }}>
					<View style={{ width: '45%', marginRight: 10 }}>
						<Image
							resizeMode={'cover'}
							style={{ width: '100%', height: 100 }}
							source={{uri: this.props.thumbnail }}
						/>
					</View>
					<View style={{ width: '50%', paddingTop: 5, paddingLeft: 5 }}>
						<Text numberOfLines={2} ellipsizeMode="tail">{ this.props.name }</Text>
						<Text note>{ this.props.construction_year } - { this.props.city }</Text>
						<Text style={{ marginTop: 5, color: '#000', fontSize: 14 }} numberOfLines={2} ellipsizeMode="tail">{ this.props.description || "" }</Text>
					</View>
				</View>
			</TouchableOpacity>
		);
				// <Card style={{flex: 0, marginTop: 10}}>
				// 	<CardItem>
				// 		<Left>
				// 			<Body>
				// 				<Text>{ this.props.name }</Text>
				// 				<Text note>{ this.props.construction_year }</Text>
				// 				<Text note>{ this.props.city }</Text>
				// 			</Body>
				// 		</Left>
				// 	</CardItem>
				// 	<CardItem cardBody>
				// 		<Image
				// 			resizeMode={'cover'}
				// 			style={{ width: '100%', height: 175 }}
				// 			source={{uri: 'http://' + this.props.thumbnail }}
				// 		/>
				// 	</CardItem>
				// </Card>
	}
}

export default Project;