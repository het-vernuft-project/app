import React, { Component } from 'react';

import { Footer, FooterTab, Button, Icon, Text } from 'native-base';

import { Link } from 'react-router-native';

class Footer_ extends Component {
	constructor(props) {
		super(props);

		this.state = {
			items: [
				{ icon: { type: "FontAwesome5", name: "building" }, text: "Projecten", location: '/projects' },
				{ icon: { type: "FontAwesome5", name: "route" }, text: "Routes", location: '/routes' },
				{ icon: { type: "FontAwesome5", name: "info" }, text: "Info", location: '/' },
			]
		};
	}

	renderItems(item, i) {
		return(
			<Button key={item.text} vertical onPress={this.props.onButtonPress.bind({}, item.location)} {...(this.props.options.active === item.text || this.props.options.active === i) ? {active: true} : {}}>
				<Icon { ...item.icon } />
				<Text>{ item.text }</Text>
			</Button>
		);
	}

	render() {
		return(
			<Footer style={(this.props.options.hidden === true ? { display: 'none' } : {})}>
				<FooterTab>
					{
						this.state.items.map(this.renderItems.bind(this))
					}
				</FooterTab>
			</Footer>
		);
	}
}

export default Footer_;