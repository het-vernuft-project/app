import React, {Component} from 'react';

import { SafeAreaView, View } from 'react-native';
import { Container, Content, StyleProvider, Text, Button, Root } from 'native-base';
import { NativeRouter, Switch, Route, Redirect, withRouter, BackButton } from 'react-router-native';

import getTheme from "./native-base-theme/components";
import variables from "./native-base-theme/variables/commonColor";

import SplashScreen from 'react-native-splash-screen';

import { getItem, setItem } from './utils';

import Routes from './components/Routes';
import Header from './components/Header';
import Footer from './components/Footer';
import Loading from './components/Loading';

import config from './config.json';

export default class App extends Component {
  constructor() {
    super();
    this.state = {
      isReady: false,
      path: false,
      headerOptions: { hidden: true },
      footerOptions: { hidden: true },
      routeProps: {},
      projects: [],
    };

    this.loader = {
      hide: () => {},
      show: () => {}
    }
  }

  async load() {
    getItem("@store:path").then(r => {
      if (r !== null) {
        this.setState({ path: r });
      }
    }).catch(err => {
    }).finally(() => {
      // this.setState({ isReady: true });
      this.getProjects();
    });
  }

  async getProjects() {
    try {
      let projects = await fetch(config.api.BASE_URL + config.api.PROJECTS, { headers: { 'Accept': 'application/json' } });
      // let projects = await fetch('http://api.dev.hetvernuft.nl/projects', { headers: { 'Accept': 'application/json' } });
      if (projects.status !== 404) {
        projects = await projects.json();
        this.setState({ projects: projects.data });
      }
    } catch(e) {
      this.setState({ projects: [] });
    } finally {
      this.setState({ isReady: true });
    }
  }

  reloadProjects() {
    this.getProjects();
  }

  setHeader(options) {
    if (this.state.headerOptions !== options) {
      this.setState({ headerOptions: options });
    }
  }

  setFooter(options) {
    if (this.state.footerOptions !== options) {
      this.setState({ footerOptions: options });
    }
  }


  componentDidMount() {
    setTimeout(() => {
      SplashScreen.hide();
    }, 1000);
    this.load();
  }

  render() {
    return (
      <StyleProvider style={getTheme(variables)}>
        <Root>
          <Container>
            <Header options={this.state.headerOptions} backPress={this.state.routeProps.backPress} />
            <SafeAreaView style={{ flex: 1, backgroundColor: "#e84934" }}>
              <View style={{ backgroundColor: "#FFF", flex: 1 }}>
                <Loading ref={ref => this.loader = ref} />
                <NativeRouter>
                  {
                    this.state.isReady ?
                      <Routes
                        loader={this.loader}
                        onRouteProps={(p) => {this.setState({ routeProps: p })}}
                        path={this.state.path}
                        setHeader={this.setHeader.bind(this)}
                        setFooter={this.setFooter.bind(this)}
                        projects={this.state.projects}
                        reloadProjects={this.reloadProjects.bind(this)}
                      />
                    :
                      null
                  }
                </NativeRouter>
              </View>
            </SafeAreaView>
            <Footer options={this.state.footerOptions} onButtonPress={location => this.state.routeProps.history.replace(location)} />
          </Container>
        </Root>
      </StyleProvider>
    );
  }
}